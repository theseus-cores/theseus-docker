#!/bin/sh

function throw_help_msg {
    echo 'usage: $0 image-name uhd-branch'
    echo '  - image-name: Image name (NOT including tag) to use when building [defaults to "uhd"]'
    echo '  - uhd-branch: Valid UHD commit, branch, or tag to clone and build [defaults to "master"]'
    echo
    echo Examples:
    echo $0
    echo $0 master
    echo $0 UHD-3.13
    echo $0 v3.13.0.1
    exit
}

# Check for help string
if [ "$1" == "-h" ] || [ "$1" == "help" ] ||
   [ "$2" == "-h" ] || [ "$2" == "help" ]; then
    throw_help_msg
fi

# Default tag to "uhd"
if [ -z "$1" ]; then
    DOCKER_TAG="uhd"
else
    DOCKER_TAG=$1
fi

# Default branch to master
if [ -z "$2" ]; then
    UHD_COMMIT="master"
else
    UHD_COMMIT=$2
fi

docker build -t $DOCKER_TAG:$UHD_COMMIT-rfnoc-all \
    --build-arg UHD_COMMIT=$UHD_COMMIT \
    --build-arg UHD_ENABLE_RFNOC=1 \
    --build-arg UHD_ENABLE_B200=1 \
    --build-arg UHD_ENABLE_USRP2=1 \
    --build-arg UHD_ENABLE_X300=1 \
    --build-arg UHD_ENABLE_MPMD=1 \
    --build-arg UHD_ENABLE_N300=1 \
    --build-arg UHD_ENABLE_E320=1 \
    --build-arg UHD_ENABLE_EXAMPLES=1 \
    --build-arg UHD_ENABLE_UTILS=1 \
    .
