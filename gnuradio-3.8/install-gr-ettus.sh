#!/bin/sh

# This script configures/builds/installs gr-ettus from source
# After building, it deletes the build directory to keep image size small(ish)

set -e
set -x

# Initialize
git clone https://github.com/ettusresearch/gr-ettus.git -b ${ETTUS_COMMIT} --depth 1

# Print git configuration
cd gr-ettus
git status
git log

# Make build directory and enter
mkdir build
cd build

# Configure cmake
cmake ../

# Build and install
make -j${MAKE_THREADS}
make install
ldconfig

# Clean up intermediate build results
cd ..
rm -rf build
