# vivado-docker

Vivado installed into a docker image. Includes python to run build scripts

Does not include RFNOC code.

Note: To create a compatible "install_config" run `xsetup –b ConfigGen`
after untarring a Vivado installer

## Build instructions

You need to download the appropriate Vivado installer, co-located with the Dockerfile:
- Xilinx_Vivado_SDK_Lin_2015.4_1118_2.tar.gz
- Xilinx_Vivado_SDK_2017.4_1216_1.tar.gz

You also need a copy of the Xilinx license file you want to use, but we've
commited a default Webpack license for convenience.

The build requires a fair amount of harddrive space available since it has
to untar, install, and copy the Vivado installation (17-20 GB or so for each duplicate)

To build the image:
1. cd into desired vivado-<version> directory
2. Copy the Vivado installer file into this directory. Installers can be found online (make sure to match the installer name with what's in the Dockerfile)
3. Build the image: `./build.sh` (or manually `docker build -t theseus-cores/vivado:<version> .`)

After building, there will be an intermediate docker image that's pretty large, which
includes both the Vivado installation *and* the untarred installer... Since we
dont need the untarred installer, which basically doubles image size, we can
go ahead and delete this intermediate image.

## Running

Run the docker image, mounting local files you want to build with vivado:
```
docker run -it -h vivado -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /local/path/to/prefix:/home/vivado/prefix --name vivado-<version> vivado:<version>
```
To start and reattach to a stopped container (with the container name "vivado"):
```bash
docker start vivado-<version>
docker attach vivado-<version>
```


